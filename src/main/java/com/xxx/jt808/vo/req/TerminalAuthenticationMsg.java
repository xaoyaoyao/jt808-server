/**     
 * @Title: TerminalAuthenticationMsg.java   
 * @Package com.xxx.jt808.vo.req   
 * @Description: TODO
 * @author weiwei 
 * @date 2017年8月24日 上午11:04:24   
 * @version V1.0     
 */
package com.xxx.jt808.vo.req;

import java.util.Arrays;

import com.xxx.jt808.common.TPMSConsts;
import com.xxx.jt808.vo.PackageData;

/**
 * @ClassName: TerminalAuthenticationMsg
 * @Description: 终端鉴权消息
 * @author weiwei
 * @date 2017年8月24日 上午11:04:24
 * 
 */
public class TerminalAuthenticationMsg extends PackageData {

	private String authCode;

	public TerminalAuthenticationMsg() {
	}

	public TerminalAuthenticationMsg(PackageData packageData) {
		this();
		this.channel = packageData.getChannel();
		this.checkSum = packageData.getCheckSum();
		this.msgBodyBytes = packageData.getMsgBodyBytes();
		this.msgHeader = packageData.getMsgHeader();
		this.authCode = new String(packageData.getMsgBodyBytes(), TPMSConsts.STRING_CHARSET_GBK);
	}

	public void setAuthCode(String authCode) {
		this.authCode = authCode;
	}

	public String getAuthCode() {
		return authCode;
	}

	@Override
	public String toString() {
		return "TerminalAuthenticationMsg [authCode=" + authCode + ", msgHeader=" + msgHeader + ", msgBodyBytes=" + Arrays.toString(msgBodyBytes) + ", checkSum=" + checkSum
				+ ", channel=" + channel + "]";
	}
}
