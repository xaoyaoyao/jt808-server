/**     
 * @Title: BitOperator.java   
 * @Package com.xxx.jt808.utils   
 * @Description: TODO
 * @author weiwei 
 * @date 2017年8月24日 上午10:01:36   
 * @version V1.0     
 */
package com.xxx.jt808.utils;

import java.util.Arrays;
import java.util.List;

/**
 * @ClassName: BitOperator
 * @Description: 字符Bit工具类
 * @author weiwei
 * @date 2017年8月24日 上午10:01:36
 * 
 */
public class BitOperator {

	/**
	 * @Title: integerTo1Byte
	 * @Description: 把一个整形该为byte
	 * @param value
	 * @Reutrn byte
	 */
	public static byte integerTo1Byte(int value) {
		return (byte) (value & 0xFF);
	}

	/**
	 * @Title: integerTo1Bytes
	 * @Description: 把一个整形该为1位的byte数组
	 * @param value
	 * @Reutrn byte[]
	 */
	public static byte[] integerTo1Bytes(int value) {
		byte[] result = new byte[1];
		result[0] = (byte) (value & 0xFF);
		return result;
	}

	/**
	 * @Title: integerTo2Bytes
	 * @Description: 把一个整形改为2位的byte数组
	 * @param value
	 * @Reutrn byte[]
	 */
	public static byte[] integerTo2Bytes(int value) {
		byte[] result = new byte[2];
		result[0] = (byte) ((value >>> 8) & 0xFF);
		result[1] = (byte) (value & 0xFF);
		return result;
	}

	/**
	 * @Title: integerTo3Bytes
	 * @Description: 把一个整形改为3位的byte数组
	 * @param value
	 * @Reutrn byte[]
	 */
	public static byte[] integerTo3Bytes(int value) {
		byte[] result = new byte[3];
		result[0] = (byte) ((value >>> 16) & 0xFF);
		result[1] = (byte) ((value >>> 8) & 0xFF);
		result[2] = (byte) (value & 0xFF);
		return result;
	}

	/**
	 * @Title: integerTo4Bytes
	 * @Description: 把一个整形改为4位的byte数组
	 * @param value
	 * @Reutrn byte[]
	 */
	public static byte[] integerTo4Bytes(int value) {
		byte[] result = new byte[4];
		result[0] = (byte) ((value >>> 24) & 0xFF);
		result[1] = (byte) ((value >>> 16) & 0xFF);
		result[2] = (byte) ((value >>> 8) & 0xFF);
		result[3] = (byte) (value & 0xFF);
		return result;
	}

	/**
	 * @Title: byteToInteger
	 * @Description: 把byte[]转化位整形,通常为指令用
	 * @param value
	 * @Reutrn int
	 */
	public static int byteToInteger(byte[] value) {
		if (value == null || value.length <= 0) {
			return 0;
		}
		int result;
		if (value.length == 1) {
			result = oneByteToInteger(value[0]);
		} else if (value.length == 2) {
			result = twoBytesToInteger(value);
		} else if (value.length == 3) {
			result = threeBytesToInteger(value);
		} else if (value.length == 4) {
			result = fourBytesToInteger(value);
		} else {
			result = fourBytesToInteger(value);
		}
		return result;
	}

	/**
	 * @Title: fourBytesToInteger
	 * @Description: TODO
	 * @param value
	 * @Reutrn int
	 */
	public static int fourBytesToInteger(byte[] value) {
		if (value == null || value.length <= 0) {
			return 0;
		}
		int temp0 = value[0] & 0xFF;
		int temp1 = value[1] & 0xFF;
		int temp2 = value[2] & 0xFF;
		int temp3 = value[3] & 0xFF;
		return ((temp0 << 24) + (temp1 << 16) + (temp2 << 8) + temp3);
	}

	/**
	 * @Title: threeBytesToInteger
	 * @Description: 把一个3位的数组转化位整形
	 * @param value
	 * @Reutrn int
	 */
	public static int threeBytesToInteger(byte[] value) {
		if (value == null || value.length <= 0) {
			return 0;
		}
		int temp0 = value[0] & 0xFF;
		int temp1 = value[1] & 0xFF;
		int temp2 = value[2] & 0xFF;
		return ((temp0 << 16) + (temp1 << 8) + temp2);
	}

	/**
	 * @Title: twoBytesToInteger
	 * @Description: 把一个2位的数组转化位整形
	 * @param value
	 * @Reutrn int
	 */
	public static int twoBytesToInteger(byte[] value) {
		if (value == null || value.length <= 0) {
			return 0;
		}
		int temp0 = value[0] & 0xFF;
		int temp1 = value[1] & 0xFF;
		return ((temp0 << 8) + temp1);
	}

	/**
	 * @Title: oneByteToInteger
	 * @Description: 把一个byte转化位整形,通常为指令用
	 * @param value
	 * @Reutrn int
	 */
	public static int oneByteToInteger(byte value) {
		return (int) value & 0xFF;
	}

	/**
	 * @Title: fourBytesToLong
	 * @Description: 把一个4位的数组转化位整形
	 * @param value
	 * @throws Exception
	 * @Reutrn long
	 */
	public static long fourBytesToLong(byte[] value) throws Exception {
		if (value == null || value.length <= 0) {
			return 0;
		}
		int temp0 = value[0] & 0xFF;
		int temp1 = value[1] & 0xFF;
		int temp2 = value[2] & 0xFF;
		int temp3 = value[3] & 0xFF;
		return (((long) temp0 << 24) + (temp1 << 16) + (temp2 << 8) + temp3);
	}

	/**
	 * @Title: bytes2Long
	 * @Description: 把一个数组转化长整形
	 * @param value
	 * @Reutrn long
	 */
	public static long bytes2Long(byte[] value) {
		if (value == null || value.length <= 0) {
			return 0;
		}
		long result = 0;
		int len = value.length;
		int temp;
		for (int i = 0; i < len; i++) {
			temp = (len - 1 - i) * 8;
			if (temp == 0) {
				result += (value[i] & 0x0ff);
			} else {
				result += (value[i] & 0x0ff) << temp;
			}
		}
		return result;
	}

	/**
	 * @Title: longToBytes
	 * @Description: 把一个长整形改为byte数组
	 * @param value
	 * @Reutrn byte[]
	 */
	public static byte[] longToBytes(long value) {
		return longToBytes(value, 8);
	}

	/**
	 * @Title: longToBytes
	 * @Description: 把一个长整形改为byte数组
	 * @param value
	 * @param len
	 * @Reutrn byte[]
	 */
	public static byte[] longToBytes(long value, int len) {
		byte[] result = new byte[len];
		int temp;
		for (int i = 0; i < len; i++) {
			temp = (len - 1 - i) * 8;
			if (temp == 0) {
				result[i] += (value & 0x0ff);
			} else {
				result[i] += (value >>> temp) & 0x0ff;
			}
		}
		return result;
	}

	/**
	 * @Title: generateTransactionID
	 * @Description: 得到一个消息ID
	 * @throws Exception
	 * @Reutrn byte[]
	 */
	public static byte[] generateTransactionID() throws Exception {
		byte[] id = new byte[16];
		System.arraycopy(integerTo2Bytes((int) (Math.random() * 65536)), 0, id, 0, 2);
		System.arraycopy(integerTo2Bytes((int) (Math.random() * 65536)), 0, id, 2, 2);
		System.arraycopy(integerTo2Bytes((int) (Math.random() * 65536)), 0, id, 4, 2);
		System.arraycopy(integerTo2Bytes((int) (Math.random() * 65536)), 0, id, 6, 2);
		System.arraycopy(integerTo2Bytes((int) (Math.random() * 65536)), 0, id, 8, 2);
		System.arraycopy(integerTo2Bytes((int) (Math.random() * 65536)), 0, id, 10, 2);
		System.arraycopy(integerTo2Bytes((int) (Math.random() * 65536)), 0, id, 12, 2);
		System.arraycopy(integerTo2Bytes((int) (Math.random() * 65536)), 0, id, 14, 2);
		return id;
	}

	/**
	 * @Title: getIntIPValue
	 * @Description: 把IP拆分位int数组
	 * @param ip
	 * @throws Exception
	 * @Reutrn int[]
	 */
	public static int[] getIntIPValue(String ip) throws Exception {
		if (null == ip || ip == "") {
			return null;
		}
		if (!ip.contains(".")) {
			return null;
		}
		String[] sip = ip.split("[.]");
		if (null == sip || sip.length < 4) {
			return null;
		}
		int[] intIP = { Integer.parseInt(sip[0]), Integer.parseInt(sip[1]), Integer.parseInt(sip[2]), Integer.parseInt(sip[3]) };
		return intIP;
	}

	/**
	 * @Title: getStringIPValue
	 * @Description: 把byte类型IP地址转化位字符串
	 * @param address
	 * @throws Exception
	 * @Reutrn String
	 */
	public static String getStringIPValue(byte[] address) throws Exception {
		if (null == address || address.length < 4) {
			return null;
		}
		int first = oneByteToInteger(address[0]);
		int second = oneByteToInteger(address[1]);
		int third = oneByteToInteger(address[2]);
		int fourth = oneByteToInteger(address[3]);
		return first + "." + second + "." + third + "." + fourth;
	}

	/**
	 * @Title: concatAll
	 * @Description: 合并字节数组
	 * @param first
	 * @param rest
	 * @Reutrn byte[]
	 */
	public static byte[] concatAll(byte[] first, byte[]... rest) {
		int totalLength = first.length;
		for (byte[] array : rest) {
			if (array != null) {
				totalLength += array.length;
			}
		}
		byte[] result = Arrays.copyOf(first, totalLength);
		int offset = first.length;
		for (byte[] array : rest) {
			if (array != null) {
				System.arraycopy(array, 0, result, offset, array.length);
				offset += array.length;
			}
		}
		return result;
	}

	/**
	 * @Title: concatAll
	 * @Description: 合并字节数组
	 * @param rest
	 * @Reutrn byte[]
	 */
	public static byte[] concatAll(List<byte[]> rest) {
		int totalLength = 0;
		for (byte[] array : rest) {
			if (array != null) {
				totalLength += array.length;
			}
		}
		byte[] result = new byte[totalLength];
		int offset = 0;
		for (byte[] array : rest) {
			if (array != null) {
				System.arraycopy(array, 0, result, offset, array.length);
				offset += array.length;
			}
		}
		return result;
	}

	/**
	 * @Title: byte2Float
	 * @Description: byte转float
	 * @param bs
	 * @Reutrn float
	 */
	public static float byte2Float(byte[] bs) {
		return Float.intBitsToFloat((((bs[3] & 0xFF) << 24) + ((bs[2] & 0xFF) << 16) + ((bs[1] & 0xFF) << 8) + (bs[0] & 0xFF)));
	}

	/**
	 * @Title: byteBE2Float
	 * @Description: byte转float
	 * @param bytes
	 * @Reutrn float
	 */
	public static float byteBE2Float(byte[] bytes) {
		int l;
		l = bytes[0];
		l &= 0xff;
		l |= ((long) bytes[1] << 8);
		l &= 0xffff;
		l |= ((long) bytes[2] << 16);
		l &= 0xffffff;
		l |= ((long) bytes[3] << 24);
		return Float.intBitsToFloat(l);
	}

	/**
	 * 
	 * @Title: getCheckSum4JT808
	 * @Description: TODO
	 * @param bs
	 * @param start
	 * @param end
	 * @Reutrn int
	 */
	public static int getCheckSum4JT808(byte[] bs, int start, int end) {
		if (start < 0 || end > bs.length)
			throw new ArrayIndexOutOfBoundsException("getCheckSum4JT808 error : index out of bounds(start=" + start + ",end=" + end + ",bytes length=" + bs.length + ")");
		int cs = 0;
		for (int i = start; i < end; i++) {
			cs ^= bs[i];
		}
		return cs;
	}

	/**
	 * @Title: getBitRange
	 * @Description: TODO
	 * @param number
	 * @param start
	 * @param end
	 * @Reutrn int
	 */
	public static int getBitRange(int number, int start, int end) {
		if (start < 0)
			throw new IndexOutOfBoundsException("min index is 0,but start = " + start);
		if (end >= Integer.SIZE)
			throw new IndexOutOfBoundsException("max index is " + (Integer.SIZE - 1) + ",but end = " + end);

		return (number << Integer.SIZE - (end + 1)) >>> Integer.SIZE - (end - start + 1);
	}

	/**
	 * @Title: getBitAt
	 * @Description: TODO
	 * @param number
	 * @param index
	 * @Reutrn int
	 */
	public static int getBitAt(int number, int index) {
		if (index < 0)
			throw new IndexOutOfBoundsException("min index is 0,but " + index);
		if (index >= Integer.SIZE)
			throw new IndexOutOfBoundsException("max index is " + (Integer.SIZE - 1) + ",but " + index);
		return ((1 << index) & number) >> index;
	}

	/**
	 * @Title: getBitAtS
	 * @Description: TODO
	 * @param number
	 * @param index
	 * @Reutrn int
	 */
	public static int getBitAtS(int number, int index) {
		String s = Integer.toBinaryString(number);
		return Integer.parseInt(s.charAt(index) + "");
	}

	/**
	 * @Title: getBitRangeS
	 * @Description: TODO
	 * @param number
	 * @param start
	 * @param end
	 * @Reutrn int
	 */
	public static int getBitRangeS(int number, int start, int end) {
		String s = Integer.toBinaryString(number);
		StringBuilder sb = new StringBuilder(s);
		while (sb.length() < Integer.SIZE) {
			sb.insert(0, "0");
		}
		String tmp = sb.reverse().substring(start, end + 1);
		sb = new StringBuilder(tmp);
		return Integer.parseInt(sb.reverse().toString(), 2);
	}

	/**
	 * @Title: main
	 * @Description: TODO
	 * @param args
	 * @Reutrn void
	 */
	public static void main(String[] args) {
		System.out.println(integerTo1Byte(1));
		System.out.println(integerTo1Bytes(1)[0]);
		System.out.println(integerTo2Bytes(1)[1]);
		System.out.println(integerTo3Bytes(1)[2]);
		System.out.println(integerTo4Bytes(1)[3]);
		
		System.out.println(bytes2Long(new byte[]{0x7e, 0x01, 0x02, 0x00, 0x07, 0x00, 0x00, 0x03, 0x08, 0x11, 0x7e}));
		System.out.println(byteBE2Float(new byte[]{0x7e, 0x01, 0x02, 0x00, 0x07, 0x00, 0x00, 0x03, 0x08, 0x11, 0x7e}));
		System.out.println(byteToInteger(new byte[]{0x7e, 0x01, 0x02, 0x00, 0x07, 0x00, 0x00, 0x03, 0x08, 0x11, 0x7e}));

	}
}
