/**     
 * @Title: MsgEncoder.java   
 * @Package com.xxx.jt808.service.codec   
 * @Description: TODO
 * @author weiwei 
 * @date 2017年8月24日 上午11:18:43   
 * @version V1.0     
 */
package com.xxx.jt808.service.codec;

import java.util.Arrays;

import com.xxx.jt808.common.TPMSConsts;
import com.xxx.jt808.utils.BitOperator;
import com.xxx.jt808.utils.JT808ProtocolUtils;
import com.xxx.jt808.vo.PackageData;
import com.xxx.jt808.vo.Session;
import com.xxx.jt808.vo.req.TerminalRegisterMsg;
import com.xxx.jt808.vo.resp.ServerCommonRespMsgBody;
import com.xxx.jt808.vo.resp.TerminalRegisterMsgRespBody;

/**
 * @ClassName: MsgEncoder
 * @Description: TODO
 * @author weiwei
 * @date 2017年8月24日 上午11:18:43
 * 
 */
public class MsgEncoder {

	public byte[] encode4TerminalRegisterResp(TerminalRegisterMsg req, TerminalRegisterMsgRespBody respMsgBody, int flowId) throws Exception {
		// 消息体字节数组
		byte[] msgBody = null;
		// 鉴权码(STRING) 只有在成功后才有该字段
		if (respMsgBody.getReplyCode() == TerminalRegisterMsgRespBody.SUCCESS) {
			msgBody = BitOperator.concatAll(Arrays.asList(//
					BitOperator.integerTo2Bytes(respMsgBody.getReplyFlowId()), // 流水号(2)
					new byte[] { respMsgBody.getReplyCode() }, // 结果
					respMsgBody.getReplyToken().getBytes(TPMSConsts.STRING_CHARSET_GBK)// 鉴权码(STRING)
					));
		} else {
			msgBody = BitOperator.concatAll(Arrays.asList(//
					BitOperator.integerTo2Bytes(respMsgBody.getReplyFlowId()), // 流水号(2)
					new byte[] { respMsgBody.getReplyCode() }// 错误代码
					));
		}

		// 消息头
		int msgBodyProps = JT808ProtocolUtils.generateMsgBodyProps(msgBody.length, 0b000, false, 0);
		byte[] msgHeader = JT808ProtocolUtils.generateMsgHeader(req.getMsgHeader().getTerminalPhone(), TPMSConsts.CMD_TERMINAL_REGISTER_RESP, msgBody, msgBodyProps, flowId);
		byte[] headerAndBody = BitOperator.concatAll(msgHeader, msgBody);

		// 校验码
		int checkSum = BitOperator.getCheckSum4JT808(headerAndBody, 0, headerAndBody.length - 1);
		// 连接并且转义
		return this.doEncode(headerAndBody, checkSum);
	}

	// public byte[] encode4ServerCommonRespMsg(TerminalAuthenticationMsg req,
	// ServerCommonRespMsgBody respMsgBody, int flowId) throws Exception {
	public byte[] encode4ServerCommonRespMsg(PackageData req, ServerCommonRespMsgBody respMsgBody, int flowId) throws Exception {
		byte[] msgBody = BitOperator.concatAll(Arrays.asList(//
				BitOperator.integerTo2Bytes(respMsgBody.getReplyFlowId()), // 应答流水号
				BitOperator.integerTo2Bytes(respMsgBody.getReplyId()), // 应答ID,对应的终端消息的ID
				new byte[] { respMsgBody.getReplyCode() }// 结果
				));

		// 消息头
		int msgBodyProps = JT808ProtocolUtils.generateMsgBodyProps(msgBody.length, 0b000, false, 0);
		byte[] msgHeader = JT808ProtocolUtils.generateMsgHeader(req.getMsgHeader().getTerminalPhone(), TPMSConsts.CMD_COMMON_RESP, msgBody, msgBodyProps, flowId);
		byte[] headerAndBody = BitOperator.concatAll(msgHeader, msgBody);
		// 校验码
		int checkSum = BitOperator.getCheckSum4JT808(headerAndBody, 0, headerAndBody.length - 1);
		// 连接并且转义
		return this.doEncode(headerAndBody, checkSum);
	}

	public byte[] encode4ParamSetting(byte[] msgBodyBytes, Session session) throws Exception {
		// 消息头
		int msgBodyProps = JT808ProtocolUtils.generateMsgBodyProps(msgBodyBytes.length, 0b000, false, 0);
		byte[] msgHeader = JT808ProtocolUtils.generateMsgHeader(session.getTerminalPhone(), TPMSConsts.CMD_TERMINAL_PARAM_SETTINGS, msgBodyBytes, msgBodyProps,
				session.currentFlowId());
		// 连接消息头和消息体
		byte[] headerAndBody = BitOperator.concatAll(msgHeader, msgBodyBytes);
		// 校验码
		int checkSum = BitOperator.getCheckSum4JT808(headerAndBody, 0, headerAndBody.length - 1);
		// 连接并且转义
		return this.doEncode(headerAndBody, checkSum);
	}

	private byte[] doEncode(byte[] headerAndBody, int checkSum) throws Exception {
		byte[] noEscapedBytes = BitOperator.concatAll(Arrays.asList(//
				new byte[] { TPMSConsts.PKG_DELIMITER }, // 0x7e
				headerAndBody, // 消息头+ 消息体
				BitOperator.integerTo1Bytes(checkSum), // 校验码
				new byte[] { TPMSConsts.PKG_DELIMITER }// 0x7e
				));
		// 转义
		return JT808ProtocolUtils.doEscape4Send(noEscapedBytes, 1, noEscapedBytes.length - 2);
	}
}
