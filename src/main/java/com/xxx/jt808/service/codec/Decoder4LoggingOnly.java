/**     
 * @Title: Decoder4LoggingOnly.java   
 * @Package com.xxx.jt808.service.codec   
 * @Description: TODO
 * @author weiwei 
 * @date 2017年8月24日 上午11:16:23   
 * @version V1.0     
 */
package com.xxx.jt808.service.codec;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ByteToMessageDecoder;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.xxx.jt808.utils.HexStringUtils;

/**
 * @ClassName: Decoder4LoggingOnly
 * @Description: TODO
 * @author weiwei
 * @date 2017年8月24日 上午11:16:23
 * 
 */
public class Decoder4LoggingOnly extends ByteToMessageDecoder {

	private final Logger log = LoggerFactory.getLogger(getClass());

	@Override
	protected void decode(ChannelHandlerContext ctx, ByteBuf in, List<Object> out) throws Exception {
		String hex = buf2Str(in);
		log.info("ip={},hex = {}", ctx.channel().remoteAddress(), hex);
		ByteBuf buf = Unpooled.buffer();
		while (in.isReadable()) {
			buf.writeByte(in.readByte());
		}
		out.add(buf);
	}

	private String buf2Str(ByteBuf in) {
		byte[] dst = new byte[in.readableBytes()];
		in.getBytes(0, dst);
		return HexStringUtils.toHexString(dst);
	}
}
